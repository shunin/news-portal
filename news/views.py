from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.views import View
from django.conf import settings

import random
from datetime import datetime, date
import json


class MainPage(View):
    def get(self, request):
        return redirect('/news/')

class NewsItem(View):
    def get(self, request, id):
        news_info = self.get_news_info(id)
        return render(request, "news/news_item.html", context=news_info)

    def get_news_info(self, id):
        # find news by its link id
        news = get_news_json()
        for item in news:
            if item['link'] == id:
                return item

        return {}


class NewsPage(View):
    def get(self, request):
        if request.GET.get('q'):
            news = get_news_json()
            news_filtered = []
            for item in news:
                if request.GET.get('q') in item['title']:
                    news_filtered.append(item)
            if not news_filtered:
                return render(request, "news/news_list.html", context={})
            news_by_dates = get_news_sorted_by_date(news_filtered)
        else:
            news_by_dates = get_news_sorted_by_date()  # [{date: date, news: [{news}]}]

        context = {"news_by_dates": news_by_dates}
        return render(request, "news/news_list.html", context=context)


class NewsCreation(View):
    def get(self, request):
        return render(request, "news/news_creation.html", context={})

    def post(self, request, *args, **kwargs):
        news_title = request.POST.get('title')
        news_text = request.POST.get('text')
        news_created = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        news_link = random.randint(10000000, 99999999)
        news = {"created": news_created,
                "text": news_text,
                "title": news_title,
                "link": news_link}
        update_news(news)

        return redirect('/news/')


def update_news(new_news: dict) -> None:
    news = get_news_json()
    news.append(new_news)
    with open(settings.NEWS_JSON_PATH, 'w') as news_list:
        json.dump(news, news_list)


def get_news_sorted_by_date(news_list_json=None) -> list:
    news_sorted = []
    dates = set()
    news = news_list_json or get_news_json()
    date_news = {}

    for item in news:
        date = datetime.strptime(item['created'], '%Y-%m-%d %H:%M:%S').date()
        if date not in dates:
            dates.add(date)
            date_news[str(date)] = [item]
        else:
            date_news[str(date)].append(item)
    dates = list(dates)
    dates.sort(reverse=True)

    for date in dates:
        news_sorted.append({'date': str(date), 'news': date_news[str(date)]})

    return news_sorted


def get_news_json() -> list:
    try:
        with open(settings.NEWS_JSON_PATH, 'r') as news_list:
            return json.load(news_list)
    except FileNotFoundError as e:
        print(str(e))
        return {}